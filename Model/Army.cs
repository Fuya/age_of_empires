using System.Collections.Generic;
using System.Linq;

namespace Age_of_Empires.Model {
    public class Army {

        public List<Building> Building_All = new List<Building> ();

        public List<Building> ChurchList => Building_All.Where (x => x.SetBulidingType == BuildingType.Church).ToList ();

        public List<Building> MonkPeopleList => Building_All.Where (x => x.PeopleList.Any (p => p.SetCareer == Career.Monk)).ToList ();
      public List<Building> FarmhouseList => Building_All.Where (x => x.SetBulidingType == BuildingType.Farmhouse).ToList ();

        public List<Building> FarmerPeopleList => Building_All.Where (x => x.PeopleList.Any (p => p.SetCareer == Career.Farmer)).ToList ();
       
        public List<Building> Tranining_Center => Building_All.Where (x => x.SetBulidingType == BuildingType.Tranining_Center).ToList ();


        public List<Building> KnightPeopleList => Building_All.Where (x => x.PeopleList.Any (p => p.SetCareer == Career.Knight)).ToList ();
       

    }
}