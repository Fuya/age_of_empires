﻿using System;
using Age_of_Empires.Model;

namespace Age_of_Empires {
    class Program {
        static void Main (string[] args) {

            var ArmyBulid = new Army ();
            var BuildingCreat = new Building { ID = 1, SetBulidingType = BuildingType.Church };
            BuildingCreat.PeopleList.Add (new People { ID = 1, SetCareer = Career.Monk });
            BuildingCreat.PeopleList.Add (new People { ID = 2, SetCareer = Career.Monk });
            BuildingCreat.PeopleList.Add (new People { ID = 3, SetCareer = Career.Monk });
            var BuildingCreat2 = new Building { ID = 2, SetBulidingType = BuildingType.Church };
            BuildingCreat2.PeopleList.Add (new People { ID = 4, SetCareer = Career.Monk });
            var BuildingCreat3 = new Building { ID = 3, SetBulidingType = BuildingType.Church };
            BuildingCreat3.PeopleList.Add (new People { ID = 5, SetCareer = Career.Monk });

            var BuildingCreat4 = new Building { ID = 4, SetBulidingType = BuildingType.Farmhouse };
            BuildingCreat4.PeopleList.Add (new People { ID = 1, SetCareer = Career.Farmer });
            BuildingCreat4.PeopleList.Add (new People { ID = 2, SetCareer = Career.Farmer });

            var BuildingCreat5 = new Building { ID = 5, SetBulidingType = BuildingType.Tranining_Center };
            BuildingCreat5.PeopleList.Add (new People { ID = 1, SetCareer = Career.Knight });

            ArmyBulid.Building_All.Add (BuildingCreat);
            ArmyBulid.Building_All.Add (BuildingCreat2);
            ArmyBulid.Building_All.Add (BuildingCreat3);
            ArmyBulid.Building_All.Add (BuildingCreat4);

            ArmyBulid.Building_All.Add (BuildingCreat5);
            int MonkPeopleList = 0;
            for (int i = 0; i < ArmyBulid.ChurchList.Count; i++) {
                MonkPeopleList = MonkPeopleList + ArmyBulid.ChurchList[i].PeopleList.Count;
            }
            int KnightPeopleList = 0;
            for (int i = 0; i < ArmyBulid.Tranining_Center.Count; i++) {
                KnightPeopleList = KnightPeopleList + ArmyBulid.Tranining_Center[i].PeopleList.Count;
            }
            int FarmerPeopleList = 0;
            for (int i = 0; i < ArmyBulid.FarmhouseList.Count; i++) {
                FarmerPeopleList = FarmerPeopleList + ArmyBulid.FarmhouseList[i].PeopleList.Count;
            }

            Console.WriteLine ("軍團一共有:" + FarmerPeopleList + "個農民," +
                MonkPeopleList + "個僧侶," +
                KnightPeopleList + "個騎士," +
                ArmyBulid.FarmhouseList.Count + "棟農舍," +
                ArmyBulid.ChurchList.Count + "棟教堂," +
                ArmyBulid.Tranining_Center.Count + "訓練所"
            );
        }
    }
}